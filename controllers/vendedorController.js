
const Vendedor = require("../models/Vendedor");

exports.crearVendedor = async (req,res) => {

    try{
        let vendedor;
         // creamos nuestro Vendedor
         vendedor = new Vendedor(req.body);
         await vendedor.save();
         res.send(vendedor);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarVendedor =async (req,res) =>{

try{
   const vendedor = await Vendedor.find();
  res.json(vendedor)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerVendedor = async (req,res) =>{
 try{
let vendedor = await Vendedor.findById(req.params.id);
if (!vendedor){
    res.status(404).json({msg: 'el vendedor no existe'})
}
res.json(vendedor);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarVendedor = async (req,res) =>{
    try{
        let vendedor = await Vendedor.findById(req.params.id);
        if (!vendedor){
            res.status(404).json({msg: 'el vendedor no existe'})
        }
        await Vendedor.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'vendedor eliminado con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarVendedor = async (req,res) =>{
    try{
     const {nombre, apellido, correo} = req.body;
     let vendedor = await Vendedor.findById(req.params.id); 
     if (!vendedor){
        res.status(404).json({msg: 'el vendedor no existe'})
    }
    vendedor.nombre = nombre;
    vendedor.apellido = apellido;
    vendedor.correo = correo;

    vendedor = await Vendedor.findOneAndUpdate({_id: req.params.id}, vendedor, {new:true})
    res.json(vendedor);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}