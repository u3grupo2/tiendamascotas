// Armamos las Rutas para  nuestro producto

const express = require('express');
const router = express.Router();
const vendedorController = require('../controllers/vendedorController');

// rutas CRUD

router.get('/', vendedorController.mostrarVendedor);
router.post('/', vendedorController.crearVendedor);
router.get('/:id', vendedorController.obtenerVendedor);
router.put('/:id', vendedorController.actualizarVendedor);
router.delete('/:id', vendedorController.eliminarVendedor);

module.exports = router;